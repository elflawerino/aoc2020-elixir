values = Aoc2020.Day6.Input.get()

Benchee.run(
  %{
    "Day6_part1"        => fn -> Aoc2020.Day6.Part1.do_the_thing(values) end,
    "Day6_part1_fast"   => fn -> Aoc2020.Day6.Part1.do_the_thing_fast(values) end
  }
)

Benchee.run(
  %{
    "Day6_part2"        => fn -> Aoc2020.Day6.Part2.do_the_thing(values) end,
    "Day6_part2_fast"   => fn -> Aoc2020.Day6.Part2.do_the_thing_fast(values) end
  }
)

"""

"""
