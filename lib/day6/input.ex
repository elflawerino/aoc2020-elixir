defmodule Aoc2020.Day6.Input do

  def get() do
    {:ok, input} = File.read("data/Day6.input")
    input
      |> String.split("\n\n")
  end
end
