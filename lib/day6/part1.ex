defmodule Aoc2020.Day6.Part1 do

  def do_the_thing(input) do
    input
    |> Enum.map(&(String.split(&1, "\n")))
    |> Enum.map(&count_group_answers/1)
    |> Enum.flat_map(&Map.keys/1)
    |> Enum.count()
  end

  def count_group_answers(group) do
    group
      |> Enum.flat_map(&(String.graphemes(&1)))
      |> Enum.reduce(%{}, fn(acc, val) -> Map.update(val, acc, 1, &(&1 + 1)) end)
  end

  def do_the_thing_fast(input) do
    input
    |> Enum.map(&(String.split(&1, "\n")))
    |> Enum.map(&count_group_answers_fast/1)
    |> Enum.count()
  end

  def count_group_answers_fast(group) do
    group
      |> Enum.flat_map(&(String.graphemes(&1)))
      |> Enum.uniq()
  end
end
