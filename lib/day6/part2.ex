defmodule Aoc2020.Day6.Part2 do

  def do_the_thing(input) do
    input
    |> Enum.map(&String.trim/1)
    |> Enum.filter(fn(x) -> byte_size(x) > 0 end)
    |> Enum.map(&count_group_answers/1)
    |> Enum.map(&remove_partial_answers/1)
    |> Enum.map(&length/1)
    |> Enum.sum()
  end

  def count_group_answers(group) do
    group_answers = String.split(group, "\n")
    group_size = length(group_answers)
    answers = group_answers
      |> Enum.flat_map(&(String.graphemes(&1)))
      |> Enum.reduce(%{}, fn(acc, val) -> Map.update(val, acc, 1, &(&1 + 1)) end)
    {group_size, answers}
  end

  @spec remove_partial_answers({any, any}) :: [any]
  def remove_partial_answers({group_size, answers}) do
    answers
    |> Enum.filter(fn({_, val}) -> val == group_size end)
  end

  @spec do_the_thing_fast(any) :: non_neg_integer
  def do_the_thing_fast(input) do
    do_the_thing(input)
  end
end
