defmodule Aoc2020.Day1.Part2 do
  @magic_target 2020
  def do_the_thing(list) do
    list
      |> create_triplets()
      |> Enum.uniq_by(fn ({x,y,z}) -> x + y + z end)
      |> Enum.filter(fn ({x,y,z}) -> x + y + z == @magic_target end)
      |> Enum.map(fn ({x,y,z}) -> x * y * z end)
      |> List.first()
  end

  defp create_triplets(list) do
    # Could be optimized by not creating duplicate pairs
    for x <- list, y <- list, z <- list, do: {x, y, z}
  end

  def do_the_thing_fast(list) do
    try do
      for x <- list, y <- list, z <- list do
        if x + y + z == @magic_target do
          # Break out of the comprehension as soon as we find a match
          throw {x, y, z}
        end
      end
    catch
      {x, y, z} -> x * y * z
    end
  end
end
