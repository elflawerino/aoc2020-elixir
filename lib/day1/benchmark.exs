values = Aoc2020.Day1.Input.get()

Benchee.run(
  %{
    "day1_part1"        => fn -> Aoc2020.Day1.Part1.do_the_thing(values) end,
    "day1_part1_fast"   => fn -> Aoc2020.Day1.Part1.do_the_thing_fast(values) end
  }
)

Benchee.run(
  %{
    "day1_part2"        => fn -> Aoc2020.Day1.Part2.do_the_thing(values) end,
    "day1_part2_fast"   => fn -> Aoc2020.Day1.Part2.do_the_thing_fast(values) end
  }
)

"""
Operating System: Linux
CPU Information: AMD Ryzen 5 3600X 6-Core Processor
Number of Available Cores: 12
Available memory: 25.01 GB
Elixir 1.11.2
Erlang 23.1.4

Benchmark suite executing with the following configuration:
warmup: 2 s
time: 5 s
memory time: 0 ns
parallel: 1
inputs: none specified
Estimated total run time: 14 s

Name                      ips        average  deviation         median         99th %
day1_part1_fast       25.87 K      0.0387 ms    ±34.07%      0.0352 ms      0.0991 ms
day1_part1             0.28 K        3.57 ms    ±15.08%        3.43 ms        5.42 ms
Comparison:
day1_part1_fast       25.87 K
day1_part1             0.28 K - 92.23x slower +3.53 ms

Name                      ips        average  deviation         median         99th %
day1_part2_fast         27.77       0.0360 s    ±17.47%       0.0346 s       0.0574 s
day1_part2               0.87         1.15 s    ±41.90%         0.94 s         2.01 s
Comparison:
day1_part2_fast         27.77
day1_part2               0.87 - 32.00x slower +1.12 s
"""
