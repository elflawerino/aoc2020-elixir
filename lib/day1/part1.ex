defmodule Aoc2020.Day1.Part1 do
  @magic_target 2020
  def do_the_thing(list) do
    list
      |> create_pairs()
      |> Enum.uniq_by(fn ({x,y}) -> x + y end)
      |> Enum.filter(fn ({x,y}) -> x + y == @magic_target end)
      |> Enum.map(fn ({x,y}) -> x * y end)
      |> List.first()
  end

  defp create_pairs(list) do
    # Could be optimized by not creating duplicate pairs
    for x <- list, y <- list, do: {x, y}
  end

  def do_the_thing_fast(list) do
    try do
      for x <- list, y <- list do
        if x + y == @magic_target do
          # Break out of the comprehension as soon as we find a match
          throw {x, y}
        end
      end
    catch
      {x, y} -> x * y
    end
  end
end
