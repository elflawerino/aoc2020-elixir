defmodule Aoc2020.Day1.Input do
  def get() do
    {:ok, day1_input} = File.read("data/Day1.input")
    day1_input
      |> String.split("\n")
      |> Enum.map(&String.to_integer/1)
  end
end
