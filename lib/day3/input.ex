defmodule Aoc2020.Day3.Input do

  alias Aoc2020.Day3.TobogganMap
  def get() do
    {:ok, input} = File.read("data/Day3.input")
    input
      |> String.split("\n")
      |> TobogganMap.parse()
  end
end
