defmodule Aoc2020.Day3.Part1 do

  alias Aoc2020.Day3.TobogganMap
  def do_the_thing(input) do
    try do
      ride_slope(input, 3, 1)
    catch
      x -> x
    end
  end

  def ride_slope(map, slope_x, slope_y, trees \\ 0, pos_x \\ 0, pos_y \\ 0)
  def ride_slope(%TobogganMap{} = map, slope_x, slope_y, trees, pos_x, pos_y) do
    {new_x, new_y} = {pos_x + slope_x, pos_y + slope_y}
    try do
      if TobogganMap.is_tree?(map, new_x, new_y) do
        ride_slope(map, slope_x, slope_y, trees + 1, new_x, new_y)
      else
        ride_slope(map, slope_x, slope_y, trees, new_x, new_y)
      end
    catch
      :oob -> throw trees
    end
  end

  def do_the_thing_fast(input) do
    do_the_thing(input)
  end
end
