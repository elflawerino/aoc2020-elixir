defmodule Aoc2020.Day3.Worker do
  use Agent

  alias Aoc2020.Day3.Part1
  alias Aoc2020.Day3.Part2
  alias Aoc2020.Day3.Input

  def start_link(_) do
    Agent.start_link(fn ->
      values = Input.get()
      IO.inspect Part1.do_the_thing(values)
      IO.inspect Part1.do_the_thing_fast(values)
      IO.inspect Part2.do_the_thing(values)
      IO.inspect Part2.do_the_thing_fast(values)
    end, name: __MODULE__)
  end
end
