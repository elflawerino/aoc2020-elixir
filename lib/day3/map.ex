defmodule Aoc2020.Day3.TobogganMap do
  defstruct map: nil,
            max_x: nil,
            max_y: nil

  alias Aoc2020.Day3.TobogganMap

  def parse(input) do
    %TobogganMap{
      map: input,
      max_x: Enum.at(input, 0) |> String.length,
      max_y: length(input)
    }
  end

  def is_tree?(%TobogganMap{map: map, max_x: max_x, max_y: max_y}, x, y) do
    if y >= max_y, do: throw :oob

    #IO.puts("Tobogganin' at #{x}, #{y}")
    char_at_coords = Enum.at(map, y)
      |> String.graphemes()
      |> Enum.at(Integer.mod(x, max_x))

    if char_at_coords == "#" do
      true
    else
      false
    end
  end
end
