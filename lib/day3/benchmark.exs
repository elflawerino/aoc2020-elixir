values = Aoc2020.Day3.Input.get()

Benchee.run(
  %{
    "day3_part1"        => fn -> Aoc2020.Day3.Part1.do_the_thing(values) end,
    "day3_part1_fast"   => fn -> Aoc2020.Day3.Part1.do_the_thing_fast(values) end
  }
)

Benchee.run(
  %{
    "day3_part2"        => fn -> Aoc2020.Day3.Part2.do_the_thing(values) end,
    "day3_part2_fast"   => fn -> Aoc2020.Day3.Part2.do_the_thing_fast(values) end
  }
)

"""
Operating System: Linux
CPU Information: AMD Ryzen 5 3600X 6-Core Processor
Number of Available Cores: 12
Available memory: 25.01 GB
Elixir 1.11.2
Erlang 23.1.4

Name                      ips        average  deviation         median         99th %
day3_part1_fast        867.32        1.15 ms     ±4.09%        1.15 ms        1.39 ms
day3_part1             863.66        1.16 ms     ±3.31%        1.15 ms        1.29 ms

Comparison:
day3_part1_fast        867.32
day3_part1             863.66 - 1.00x slower +0.00490 ms

Name                      ips        average  deviation         median         99th %
day3_part2             192.28        5.20 ms     ±2.53%        5.19 ms        5.64 ms
day3_part2_fast        189.87        5.27 ms     ±2.37%        5.24 ms        5.77 ms

Comparison:
day3_part2             192.28
day3_part2_fast        189.87 - 1.01x slower +0.0659 ms
"""
