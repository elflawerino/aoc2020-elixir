defmodule Aoc2020.Day8.Part2 do
  @debug false

  def do_the_thing(input) do
    instructions = input
    |> Enum.with_index(1)
    |> Enum.map(&parse_instruction/1)
    |> Enum.reduce(%{}, fn({index, instruction}, acc) -> Map.put(acc, index, instruction) end)

    try do
      execute(instructions, 1, map_size(instructions) + 1, false)
    catch
      x -> x
    end
  end

  def execute(instructions, current, target, in_branch \\ false, branches \\ 0, acc \\ 0, visited_instructions \\ [])
  def execute(instructions, current, target, in_branch, branches, acc, visited_instructions) do
    if current == target do
      indent_puts(branches, "Found #{target}, acc is #{acc}")
      throw acc
    end

    if not Map.has_key?(instructions, current) do
      indent_puts(branches, "Rejecting #{current} as outside of address space")
      throw :wrong
    end

    {instruction, value} = Map.get(instructions, current)
    indent_puts(branches, "Executing (#{current}: #{instruction} #{value}) -> #{acc}")
    cond do
      current in visited_instructions ->
        indent_puts(branches, "Rejecting (#{current}: #{instruction} #{value}) -> #{acc} as looping")
        throw :wrong
      true -> case instruction do
        "jmp" ->
          try do
            do_jmp(instructions, current, target, in_branch, branches, acc, visited_instructions, value)
          catch
            :wrong ->
              if in_branch do
                indent_puts(branches, "Rejecting (#{current}: #{instruction} #{value}) -> #{acc}  as requiring multiple switches")
                throw :wrong
              end
              indent_puts(branches, "Changing (#{current}: #{instruction} #{value}) -> nop")
              do_nop(instructions, current, target, true, branches + 1, acc, visited_instructions, value)
          end
        "nop" ->
          try do
            do_nop(instructions, current, target, in_branch, branches, acc, visited_instructions, value)
          catch
            :wrong ->
              if in_branch do
                indent_puts(branches, "Rejecting (#{current}: #{instruction} #{value})  -> #{acc}  as requiring multiple switches")
                throw :wrong
              end
              indent_puts(branches, "Changing (#{current}: #{instruction} #{value}) -> jmp")
              do_jmp(instructions, current, target, true, branches + 1, acc, visited_instructions, value)
          end
        "acc" -> do_acc(instructions, current, target, in_branch, branches, acc, visited_instructions, value)
      end
    end
  end

  def do_jmp(instructions, current, target, in_branch, branches, acc, visited_instructions, value) do
    execute(instructions, current + value, target, in_branch, branches, acc, [current | visited_instructions])
  end

  def do_acc(instructions, current, target, in_branch, branches, acc, visited_instructions, value) do
    execute(instructions, current + 1, target, in_branch, branches, acc + value, [current | visited_instructions])
  end

  def do_nop(instructions, current, target, in_branch, branches, acc, visited_instructions, _) do
    execute(instructions, current + 1, target, in_branch, branches, acc, [current | visited_instructions])
  end

  def indent_puts(indent, message) do
    if @debug do
      IO.puts("#{String.duplicate("+", indent)}#{message}")
    end
  end

  def parse_instruction({instruction, index}) do
    [instruction, value] = String.split(instruction, " ")
    {int_value, _} = Integer.parse(value)
    {index, {instruction, int_value}}
  end

  ### "fast" variants below this line ###
  def do_the_thing_fast(input) do
    instructions = input
    |> Enum.with_index(1)
    |> Enum.map(&parse_instruction_f/1)
    |> Enum.reduce(%{}, fn({index, instruction}, acc) -> Map.put(acc, index, instruction) end)

    try do
      execute_f(instructions, 1, map_size(instructions) + 1, false)
    catch
      x -> x
    end
  end

  @jmp 0
  @acc 1
  @nop 2

  def parse_instruction_f({instruction, index}) do
    [instruction, value] = String.split(instruction, " ")
    {int_value, _} = Integer.parse(value)
    encoded = case instruction do
      # Changing these from strings to ints made this about 10% faster
      "jmp" -> @jmp
      "acc" -> @acc
      "nop" -> @nop
    end
    {index, {encoded, int_value}}
  end

  def execute_f(instructions, current, target, in_branch \\ false, acc \\ 0, visited_instructions \\ [])
  def execute_f(instructions, current, target, in_branch,acc, visited_instructions) do
    if current == target do
      throw acc
    end

    if not Map.has_key?(instructions, current) do
      throw :wrong
    end

    cond do
      current in visited_instructions -> throw :wrong
      true ->
        {instruction, value} = Map.get(instructions, current)
        case instruction do
        @jmp ->
          try do
            execute_f(instructions, current + value, target, in_branch, acc, [current | visited_instructions])
          catch
            :wrong ->
              if in_branch, do: throw :wrong
              execute_f(instructions, current + 1, target, true, acc, [current | visited_instructions])
          end
        @nop ->
          try do
            execute_f(instructions, current + 1, target, in_branch, acc, [current | visited_instructions])
          catch
            :wrong ->
              if in_branch, do: throw :wrong
              execute_f(instructions, current + value, target, true, acc, [current | visited_instructions])
          end
        @acc -> execute_f(instructions, current + 1, target, in_branch, acc + value, [current | visited_instructions])
      end
    end
  end
end
