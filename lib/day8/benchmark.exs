values = Aoc2020.Day8.Input.get()

Benchee.run(
  %{
    "Day8_part1"        => fn -> Aoc2020.Day8.Part1.do_the_thing(values) end,
    "Day8_part1_fast"   => fn -> Aoc2020.Day8.Part1.do_the_thing_fast(values) end
  }
)

Benchee.run(
  %{
    "Day8_part2"        => fn -> Aoc2020.Day8.Part2.do_the_thing(values) end,
    "Day8_part2_fast"   => fn -> Aoc2020.Day8.Part2.do_the_thing_fast(values) end
  }
)

"""
Operating System: macOS
CPU Information: Intel(R) Core(TM) i7-9750H CPU @ 2.60GHz
Number of Available Cores: 12
Available memory: 16 GB
Elixir 1.11.2
Erlang 23.1.4

Name                      ips        average  deviation         median         99th %
Day8_part1_fast        1.10 K      907.28 μs    ±11.84%      893.90 μs     1220.98 μs
Day8_part1             1.01 K      989.41 μs    ±16.70%      952.90 μs     1580.57 μs

Comparison:
Day8_part1_fast        1.10 K
Day8_part1             1.01 K - 1.09x slower +82.13 μs

Name                      ips        average  deviation         median         99th %
Day8_part2_fast        1.08 K        0.92 ms    ±10.86%        0.92 ms        1.24 ms
Day8_part2             0.51 K        1.96 ms     ±8.24%        1.94 ms        2.38 ms

Comparison:
Day8_part2_fast        1.08 K
Day8_part2             0.51 K - 2.12x slower +1.04 ms
"""
