defmodule Aoc2020.Day8.Input do

  def get() do
    {:ok, input} = File.read("data/Day8.input")
    input
      |> String.split("\n")
  end
end
