defmodule Aoc2020.Day8.Part1 do

  def do_the_thing(input) do
    input
    |> Enum.with_index()
    |> Enum.map(&parse_instruction/1)
    |> Enum.reduce(%{}, fn(elem, acc) -> Map.merge(acc, elem) end)
    |> execute()
  end

  def execute(instructions, current \\ 0, acc \\ 0, visited_instructions \\ [])
  def execute(instructions, current, acc, visited_instructions) do
    {instruction, value} = Map.get(instructions, current)
    cond do
      current in visited_instructions -> acc
      true -> case instruction do
        "jmp" -> execute(instructions, current + value, acc, [current | visited_instructions])
        "acc" -> execute(instructions, current + 1, acc + value, [current | visited_instructions])
        "nop" -> execute(instructions, current + 1, acc, [current | visited_instructions])
      end
    end
  end

  def parse_instruction({instruction, index}) do
    [instruction, value] = String.split(instruction, " ")
    {int_value, _} = Integer.parse(value)
    Map.put(%{}, index, {instruction, int_value})
  end

  def parse_instruction_f({instruction, index}) do
    [instruction, value] = String.split(instruction, " ")
    {int_value, _} = Integer.parse(value)
    {index, {instruction, int_value}}
  end

  def do_the_thing_fast(input) do
    input
    |> Enum.with_index()
    |> Enum.map(&parse_instruction_f/1)
    |> Enum.reduce(%{}, fn({index, instruction}, acc) -> Map.put(acc, index, instruction) end)
    |> execute()  end
end
