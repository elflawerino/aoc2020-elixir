defmodule Aoc2020.Day5.Part2 do

  def do_the_thing(input) do
    input
    |> Enum.map(&String.trim/1)
    |> Enum.filter(fn(x) -> byte_size(x) > 0 end)
    |> Enum.map(fn(x) -> String.split_at(x, 7) end)
    |> Enum.map(&find_seat/1)
    |> Enum.map(&calculate_seat_id/1)
    |> Enum.sort()
    |> find_own_seat()
  end

  def find_own_seat([head | tail]) do
    [head_plus_one | tail] = tail
    if head_plus_one - head == 2 do
      head + 1
    else
      find_own_seat(tail)
    end
  end

  def calculate_seat_id(%{:row => row, :seat => seat}), do: row * 8 + seat

  def find_seat(row_and_seat, total_rows \\ 127, total_seats \\ 7)
  def find_seat({row_code, seat_code}, total_rows, total_seats) do
    %{
      row: find_row(row_code, 0..total_rows),
      seat: find_seat_number(seat_code, 0..total_seats)
    }
  end

  def find_row(<<row_instruction, rest::binary>>, row_range) do
    #IO.inspect row_range
    cond do
      row_instruction == ?F -> find_row(rest, lower_half(row_range))
      row_instruction == ?B -> find_row(rest, upper_half(row_range))
      true -> IO.inspect row_instruction
    end
  end

  def find_row(_, row) when is_integer(row) do
    row
  end

  def find_seat_number(<<seat_instruction, rest::binary>>, seat_range) do
    cond do
      seat_instruction == ?L -> find_seat_number(rest, lower_half(seat_range))
      seat_instruction == ?R -> find_seat_number(rest, upper_half(seat_range))
      true -> IO.inspect seat_instruction
    end
  end

  def find_seat_number(_, seat) when is_integer(seat) do
    seat
  end

  def lower_half(lower..upper) do
    cond do
      Enum.count(lower..upper) == 2 -> lower
      Enum.count(lower..upper) == 1 -> lower
      true -> lower..midpoint(lower..upper)-1
    end
  end

  def upper_half(lower..upper) do
    cond do
      Enum.count(lower..upper) == 2 -> upper
      Enum.count(lower..upper) == 1 -> upper
      true -> midpoint(lower..upper)..upper
    end

  end

  def midpoint(lower..upper), do: trunc(Enum.count(lower..upper) / 2) + lower

  def do_the_thing_fast(input) do
    input
    |> Enum.filter(fn(x) -> byte_size(x) > 0 end)
    |> Enum.map(fn(x) -> String.split_at(x, 7) end)
    |> Enum.map(&find_seat/1)
    |> Enum.map(&calculate_seat_id/1)
    |> Enum.sort()
    |> find_own_seat()
  end
end
