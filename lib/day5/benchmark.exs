values = Aoc2020.Day6.Input.get()

Benchee.run(
  %{
    "Day6_part1"        => fn -> Aoc2020.Day6.Part1.do_the_thing(values) end,
    "Day6_part1_fast"   => fn -> Aoc2020.Day6.Part1.do_the_thing_fast(values) end
  }
)

Benchee.run(
  %{
    "Day5_part2"        => fn -> Aoc2020.Day5.Part2.do_the_thing(values) end,
    "Day5_part2_fast"   => fn -> Aoc2020.Day5.Part2.do_the_thing_fast(values) end
  }
)

"""
Operating System: Linux
CPU Information: AMD Ryzen 5 3600X 6-Core Processor
Number of Available Cores: 12
Available memory: 25.01 GB
Elixir 1.11.2
Erlang 23.1.4

Name                      ips        average  deviation         median         99th %
Day6_part1_fast        236.02        4.24 ms     ±3.64%        4.19 ms        4.96 ms
Day6_part1             220.03        4.54 ms     ±7.03%        4.47 ms        6.24 ms

Comparison:
Day6_part1_fast        236.02
Day6_part1             220.03 - 1.07x slower +0.31 ms

Name                      ips        average  deviation         median         99th %
Day6_part2_fast        233.99        4.27 ms     ±2.67%        4.25 ms        4.83 ms
Day6_part2             219.42        4.56 ms     ±3.35%        4.53 ms        5.21 ms

Comparison:
Day6_part2_fast        233.99
Day6_part2             219.42 - 1.07x slower +0.28 ms
"""
