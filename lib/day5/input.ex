defmodule Aoc2020.Day5.Input do

  def get() do
    {:ok, input} = File.read("data/Day5.input")
    input
      |> String.split("\n")
  end
end
