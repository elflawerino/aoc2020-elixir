defmodule Aoc2020.Day7.Part2 do

  def do_the_thing(input) do
    map = input
    |> Enum.flat_map(&parse_rule/1)
    |> Enum.reduce(%{}, fn({key, val}, acc) -> Map.put(acc, key, val) end)

    # -1 because we don't want to count the "shiny gold" bag
    count_bag_size(map, {"shiny gold", Map.get(map, "shiny gold")}) - 1
  end

  def count_bag_size(all_rules, rule)
  def count_bag_size(all_rules, {_, children}) when is_map(children) do
    Enum.reduce(children, 1, fn({c, n}, acc) ->
      acc + (n * count_bag_size(all_rules, {c, Map.get(all_rules, c)}))
    end)
  end

  # A bag with no children has a size of 1
  def count_bag_size(_, _), do: 1

  def parse_rule(rule) do
    %{"container" => container, "children" => children} = Regex.named_captures(~r/^(?<container>.+) bags contain (?<children>.*).$/, rule)

    cond do
      children =~ "no other" -> Map.put(%{}, container, :end)
      true ->
        parsed_children =
          children
          |> String.split(", ")
          |> Enum.map(fn(child) -> Regex.named_captures(~r/(?<amount>\d) (?<color>.+) bag[s]?$/, child) end)
          |> Enum.map(fn(%{"amount" => amount, "color" => color}) ->
            {value, _} = Integer.parse(amount)
            {color, value}
          end)
          |> Enum.reduce(%{}, fn({color, amount}, acc) -> Map.put(acc, color, amount) end)
        Map.put(%{}, container, parsed_children)
    end
  end

  def do_the_thing_fast(input) do
    do_the_thing(input)
  end
end
