defmodule Aoc2020.Day7.Input do

  def get() do
    {:ok, input} = File.read("data/Day7.input")
    input
      |> String.split("\n")
  end
end
