defmodule Aoc2020.Day7.Part1 do

  def do_the_thing(input) do
    map = input
    |> Enum.flat_map(&parse_rule/1)
    |> Enum.reduce(%{}, fn({key, val}, acc) -> Map.put(acc, key, val) end)

    find_suitable_containers(map, "shiny gold")
    |> Enum.count()
  end

  @spec find_suitable_containers(any, any) :: [any]
  def find_suitable_containers(bag_rules, our_bag)
  def find_suitable_containers(bag_rules, our_bag) do
    bag_rules
    |> Enum.filter(&(is_suitable?(bag_rules, &1, our_bag)))
    |> Enum.map(fn({key, _}) -> key end)
  end

  def is_suitable?(all_rules, rule, our_bag)
  def is_suitable?(all_rules, {_, children}, our_bag) when is_list(children) do
    can_hold_our_bag = children
      |> Enum.flat_map(&Map.keys/1)
      |> Enum.any?(&(&1 =~ our_bag))

    can_hold_our_bag = cond do
      can_hold_our_bag -> true
      not can_hold_our_bag -> Enum.flat_map(children, &Map.keys/1)
                              |> Enum.any?(&(is_suitable?(all_rules, {&1, Map.get(all_rules, &1)}, our_bag)))
    end

    can_hold_our_bag
  end

  def is_suitable?(_, {parent, children}, our_bag) when children == :end do
    parent == our_bag
  end

  def parse_rule(rule) do
    %{"container" => container, "children" => children} = Regex.named_captures(~r/^(?<container>.+) bags contain (?<children>.*).$/, rule)

    cond do
      children =~ "no other" -> Map.put(%{}, container, :end)
      true -> parsed_children = children
                                  |> String.split(", ")
                                  |> Enum.map(fn(child) -> Regex.named_captures(~r/(?<amount>\d) (?<color>.+) bag[s]?$/, child) end)
                                  |> Enum.map(fn(%{"amount" => amount, "color" => color}) -> Map.put(%{}, color, amount) end)
                                  Map.put(%{}, container, parsed_children)
    end
  end

  def do_the_thing_fast(input) do
    do_the_thing(input)
  end
end
