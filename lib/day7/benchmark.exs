values = Aoc2020.Day7.Input.get()

Benchee.run(
  %{
    "Day7_part1"        => fn -> Aoc2020.Day7.Part1.do_the_thing(values) end,
    "Day7_part1_fast"   => fn -> Aoc2020.Day7.Part1.do_the_thing_fast(values) end
  }
)

Benchee.run(
  %{
    "Day7_part2"        => fn -> Aoc2020.Day7.Part2.do_the_thing(values) end,
    "Day7_part2_fast"   => fn -> Aoc2020.Day7.Part2.do_the_thing_fast(values) end
  }
)

"""
Operating System: macOS
CPU Information: Intel(R) Core(TM) i7-9750H CPU @ 2.60GHz
Number of Available Cores: 12
Available memory: 16 GB
Elixir 1.11.2
Erlang 23.1.4

Name                      ips        average  deviation         median         99th %
Day7_part1               7.79      128.32 ms     ±4.92%      127.80 ms      157.84 ms
Day7_part1_fast          7.79      128.37 ms     ±2.52%      127.52 ms      142.29 ms

Comparison:
Day7_part1               7.79
Day7_part1_fast          7.79 - 1.00x slower +0.0535 ms

Name                      ips        average  deviation         median         99th %
Day7_part2             102.53        9.75 ms    ±29.74%        9.00 ms       26.11 ms
Day7_part2_fast        102.51        9.76 ms    ±21.22%        9.08 ms       21.06 ms

Comparison:
Day7_part2             102.53
Day7_part2_fast        102.51 - 1.00x slower +0.00236 ms
"""
