values = Aoc2020.Day9.Input.get()

Benchee.run(
  %{
    "Day9_part1"        => fn -> Aoc2020.Day9.Part1.do_the_thing(values) end,
    #"Day9_part1_fast"   => fn -> Aoc2020.Day9.Part1.do_the_thing_fast(values) end
  }
)

Benchee.run(
  %{
    "Day9_part2"        => fn -> Aoc2020.Day9.Part2.do_the_thing(values) end,
    #"Day9_part2_fast"   => fn -> Aoc2020.Day9.Part2.do_the_thing_fast(values) end
  }
)

"""
CPU Information: Intel(R) Core(TM) i7-9750H CPU @ 2.60GHz
Number of Available Cores: 12
Available memory: 16 GB
Elixir 1.11.2
Erlang 23.1.4

Name                 ips        average  deviation         median         99th %
Day9_part1          0.22         4.59 s     ±2.14%         4.59 s         4.66 s

Name                 ips        average  deviation         median         99th %
Day9_part2        164.23        6.09 ms    ±10.79%        5.96 ms        8.83 ms
"""
