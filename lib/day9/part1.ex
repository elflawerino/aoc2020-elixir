defmodule Aoc2020.Day9.Part1 do

  def do_the_thing(input) do
    input
    |> Enum.map(&String.to_integer/1)
    |> generate_chunks()
    |> Enum.find(:not_found, &is_incorrect?/1)
  end

  def is_incorrect?(input) do
    not is_correct?(input)
  end

  def is_correct?({preceding, target}) do
    pairs = for x <- preceding, y <- preceding do
      {x, y}
    end

    pairs
    |> Enum.any?(fn({x, y}) -> x + y == target end)
  end

  def generate_chunks(input) do
    preamble = Enum.take(input, 24)
    first_value = Enum.at(input, 25)
    first_chunk = {preamble, first_value}

    chunks = Stream.iterate(26, &(&1 + 1))
    |> Enum.take(length(input))
    |> Enum.map(fn(x) ->
      chunk = Enum.slice(input, x - 25, x)
      target = Enum.at(input, x)
      {chunk, target}
    end)

    [first_chunk | chunks]
  end

  def do_the_thing_fast(input) do

  end
end
