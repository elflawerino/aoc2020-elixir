defmodule Aoc2020.Day9.Input do

  def get() do
    {:ok, input} = File.read("data/Day9.input")
    input
      |> String.split("\n")
  end

  def get_test() do
    {:ok, input} = File.read("data/Day9_test.input")
    input
      |> String.split("\n")
  end
end
