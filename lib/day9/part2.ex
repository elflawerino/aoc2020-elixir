defmodule Aoc2020.Day9.Part2 do
  @magic 177777905
  @magic2 127

  def do_the_thing(input) do
    input_numbers = input
    |> Enum.map(&String.to_integer/1)
    |> Enum.filter(&(&1 < @magic))

    try do
      find_contiguous_set(input_numbers, 0, 1)
    catch
      {p1, p2} ->
        range = Enum.slice(input_numbers, p1, p2 - p1)
        Enum.min(range) + Enum.max(range)
    end
  end

  def find_contiguous_set(input, p1, p2, sum \\ 0)
  def find_contiguous_set(input, p1, p2, sum) when sum <= 0 do
    p1_val = Enum.at(input, p1)
    p2_val = Enum.at(input, p2)

    find_contiguous_set(input, p1, p2, p1_val + p2_val)
  end

  def find_contiguous_set(input, p1, p2, sum) when p2 <= length(input) do
    p1_val = Enum.at(input, p1)
    p2_val = Enum.at(input, p2 + 1)

    cond do
      sum == @magic -> throw {p1, p2}
      sum > @magic -> find_contiguous_set(input, p1 + 1, p2, sum - p1_val)
      sum < @magic -> find_contiguous_set(input, p1, p2 + 1, sum + p2_val)
    end
  end


  def do_the_thing_fast(input) do

  end
end
