defmodule Aoc2020.Day4.Part1 do

  def do_the_thing(input) do
    input
      |> Enum.filter(&is_valid?/1)
      |> Enum.count()
  end

  def is_valid?(%{"byr" => _, "ecl" => _, "eyr" => _, "hcl" => _, "hgt" => _, "iyr" => _, "pid" => _}) do
    true
  end

  def is_valid?(%{} = _map) do
    false
  end

  def do_the_thing_fast(input) do
    do_the_thing(input)
  end
end
