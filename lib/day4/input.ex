defmodule Aoc2020.Day4.Input do

  def get() do
    {:ok, input} = File.read("data/Day4.input")
    input
      |> String.split("\n\n")
      |> Enum.map(fn(x) -> String.split(x, " ") end)
      |> Enum.map(fn(x) ->
          Enum.map(x, fn(x) -> String.split(x, "\n") end)
          |> Enum.flat_map(fn(x) -> x end)
          |> Enum.filter(fn(x) -> String.length(x) > 0 end)
          |> Enum.reduce(%{}, fn(pair, map) ->
              [key, value] = String.split(pair, ":")
              Map.put(map, key, value)
            end)
        end)
  end
end
