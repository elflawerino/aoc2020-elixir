defmodule Aoc2020.Day4.Part2 do

  def do_the_thing(input) do
    input
      |> Enum.filter(&is_valid?/1)
      |> Enum.count()
  end

  def is_valid?(%{"byr" => byr, "ecl" => ecl, "eyr" => eyr, "hcl" => hcl, "hgt" => hgt, "iyr" => iyr, "pid" => pid}) do
    is_byr_valid?(byr)
    and is_eye_color_valid?(ecl)
    and is_eyr_valid?(eyr)
    and is_hair_color_valid?(hcl)
    and is_height_valid?(hgt)
    and is_iyr_valid?(iyr)
    and is_passport_id_valid?(pid)
  end

  def is_valid?(%{}) do
    false
  end

  def is_byr_valid?(byr) do
    {byr, _} = Integer.parse(byr)
    1920 <= byr and byr <= 2002
  end

  def is_iyr_valid?(iyr) do
    {iyr, _} = Integer.parse(iyr)
    2010 <= iyr and iyr <= 2020
  end

  def is_eyr_valid?(eyr) do
    {eyr, _} = Integer.parse(eyr)
    2020 <= eyr and eyr <= 2030
  end

  def is_height_valid?(height) do
    %{"height" => height, "unit" => unit} = Regex.named_captures(~r{^(?<height>\d+)(?<unit>\S+)$}, height)
    {value, _} = Integer.parse(height)
    (unit == "cm" and 150 <= value and value <= 193) or (unit == "in" and 59 <= value and value <= 76)
  end

  def is_hair_color_valid?(code) do
    Regex.match?(~r/^#[0-9a-f]{6}$/, code)
  end

  def is_eye_color_valid?(color) do
    cond do
      color == "amb" -> true
      color == "blu" -> true
      color == "brn" -> true
      color == "gry" -> true
      color == "grn" -> true
      color == "hzl" -> true
      color == "oth" -> true
      true -> false
    end
  end

  def is_passport_id_valid?(pid) do
    String.length(pid) == 9
  end

  def do_the_thing_fast(input) do
    do_the_thing(input)
  end
end
