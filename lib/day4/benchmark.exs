values = Aoc2020.Day4.Input.get()

Benchee.run(
  %{
    "day4_part1"        => fn -> Aoc2020.Day4.Part1.do_the_thing(values) end,
    "day4_part1_fast"   => fn -> Aoc2020.Day4.Part1.do_the_thing_fast(values) end
  }
)

Benchee.run(
  %{
    "day4_part2"        => fn -> Aoc2020.Day4.Part2.do_the_thing(values) end,
    "day4_part2_fast"   => fn -> Aoc2020.Day4.Part2.do_the_thing_fast(values) end
  }
)
"""
Operating System: Linux
CPU Information: AMD Ryzen 5 3600X 6-Core Processor
Number of Available Cores: 12
Available memory: 25.01 GB
Elixir 1.11.2
Erlang 23.1.4

Name                      ips        average  deviation         median         99th %
day4_part1_fast       34.10 K       29.32 μs    ±15.93%       28.50 μs       40.90 μs
day4_part1            33.93 K       29.47 μs    ±18.04%       28.60 μs       43.50 μs

Comparison:
day4_part1_fast       34.10 K
day4_part1            33.93 K - 1.01x slower +0.150 μs

Name                      ips        average  deviation         median         99th %
day4_part2             711.30        1.41 ms     ±3.96%        1.39 ms        1.67 ms
day4_part2_fast        706.92        1.41 ms     ±4.03%        1.40 ms        1.67 ms

Comparison:
day4_part2             711.30
day4_part2_fast        706.92 - 1.01x slower +0.00871 ms
"""
