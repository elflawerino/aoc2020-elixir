defmodule Aoc2020.Application do
  use Application

  def start(_type, _args) do
    # Uncomment the days you want to run
    children = [
      #Aoc2020.Day1.Worker
      #Aoc2020.Day2.Worker
      #Aoc2020.Day3.Worker
      #Aoc2020.Day4.Worker
      #Aoc2020.Day5.Worker
      #Aoc2020.Day6.Worker
      #Aoc2020.Day7.Worker
      #Aoc2020.Day8.Worker
      #Aoc2020.Day9.Worker
    ]
    Supervisor.start_link(children, strategy: :one_for_one)
  end
end
