defmodule Aoc2020.Day2.Part1 do

  def do_the_thing(list) do
    list
      |> Enum.map(&parse/1)
      |> Enum.filter(&is_valid?/1)
      |> Enum.count()
  end

  def do_the_thing_fast(list) do
    do_the_thing(list)
  end

  defp parse(input) do
    regex = ~r{^(?<low>\d+)-(?<high>\d+) (?<char>\S): (?<pass>.*)$}
    Regex.named_captures(regex, input)
  end

  defp is_valid?(%{"low" => low, "high" => high, "char" => char, "pass" => pass}) do
    occurrences = count_characters(pass, char)
    {low, _} = Integer.parse(low)
    {high, _} = Integer.parse(high)
    if low <= occurrences and occurrences <= high do
      :true
    else
      :false
    end
  end

  defp count_characters(input_string, character) do
    input_string
      |> String.graphemes()
      |> Enum.filter(fn(x) -> x === character end)
      |> Enum.count()
  end
end
