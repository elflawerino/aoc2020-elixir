values = Aoc2020.Day2.Input.get()

Benchee.run(
  %{
    "day2_part1"        => fn -> Aoc2020.Day2.Part1.do_the_thing(values) end,
    "day2_part1_fast"   => fn -> Aoc2020.Day2.Part1.do_the_thing_fast(values) end
  }
)

Benchee.run(
  %{
    "day2_part2"        => fn -> Aoc2020.Day2.Part2.do_the_thing(values) end,
    "day2_part2_fast"   => fn -> Aoc2020.Day2.Part2.do_the_thing_fast(values) end
  }
)

"""
Operating System: Linux
CPU Information: AMD Ryzen 5 3600X 6-Core Processor
Number of Available Cores: 12
Available memory: 25.01 GB
Elixir 1.11.2
Erlang 23.1.4

Name                      ips        average  deviation         median         99th %
day2_part1_fast        180.93        5.53 ms     ±5.85%        5.41 ms        6.60 ms
day2_part1             178.29        5.61 ms    ±20.97%        5.45 ms        7.04 ms

Comparison:
day2_part1_fast        180.93
day2_part1             178.29 - 1.01x slower +0.0817 ms

Name                      ips        average  deviation         median         99th %
day2_part2_fast        197.21        5.07 ms     ±5.87%        4.95 ms        6.09 ms
day2_part2             196.21        5.10 ms     ±6.24%        4.96 ms        6.10 ms

Comparison:
day2_part2_fast        197.21
day2_part2             196.21 - 1.01x slower +0.0260 ms
"""
