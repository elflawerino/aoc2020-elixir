defmodule Aoc2020.Day2.Part2 do

  def do_the_thing(list) do
    list
      |> Enum.map(&parse/1)
      |> Enum.filter(&is_valid?/1)
      |> Enum.count()
  end

  def do_the_thing_fast(list) do
    do_the_thing(list)
  end

  def is_valid?(%{"pos1" => pos1, "pos2" => pos2, "char" => char, "pass" => pass}) do
    graphemes = String.graphemes(pass)
    {pos1, _} = Integer.parse(pos1)
    {pos2, _} = Integer.parse(pos2)
    char_at_pos1 = Enum.at(graphemes, pos1 - 1)
    char_at_pos2 = Enum.at(graphemes, pos2 - 1)
    if char_at_pos1 == char or char_at_pos2 == char do
      if char_at_pos1 == char and char_at_pos2 == char do
        :false
      else
        :true
      end
    else
      :false
    end
  end

  def parse(input) do
    regex = ~r{^(?<pos1>\d+)-(?<pos2>\d+) (?<char>\S): (?<pass>.*)$}
    Regex.named_captures(regex, input)
  end
end
