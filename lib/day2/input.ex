defmodule Aoc2020.Day2.Input do
  def get() do
    {:ok, input} = File.read("data/Day2.input")
    input
      |> String.split("\n")
  end
end
